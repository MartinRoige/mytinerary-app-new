var mongoose=require('mongoose');

const Schema=mongoose.Schema;

var activitySchema= new Schema({
    itineraryId: Schema.Types.ObjectId,
    Name: String,
    Image: String,
    commentId: {
        userId: Schema.Types.ObjectId,
        comment: String
    }
},{
    collection: 'Activities'
});

module.exports = mongoose.model('activity', activitySchema)