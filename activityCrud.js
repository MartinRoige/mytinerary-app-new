var express = require('express');
var cors = require('cors');
var activityModel = require('./activity');

const route = express.Router();
route.use(cors());

route.get('/activities', function(req, res) {
    activityModel.find().then(function(datos) {
    var activities = res.send(datos);
    return activities;
  });
});

module.exports = route;