var express = require('express');
var cors = require('cors');
var itineraryModel = require('./itinerary');

const route = express.Router();
route.use(cors());

route.get('/list/:id', function(req, res) {  
  itineraryModel.find({ cityId: req.params.id }).then(function(datos) {
    var itineraries = res.send(datos);
    return itineraries;
  });
});

module.exports = route;
