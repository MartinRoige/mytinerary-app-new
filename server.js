var express = require('express');
var mongoose = require('mongoose');
var cors = require('cors');
const cityCrud = require('./cityCrud');
const activityCrud = require('./activityCrud');
const routeUser = require('./controladorUser');
const bodyParser = require('body-parser');
const itineraryCrud = require('./itineraryCrud');
const passport = require('passport')



const authApi = require('./auth/authApis');
const app = express();
app.use(cors());
app.use(bodyParser());
app.use(passport.initialize());

mongoose.connect(
  'mongodb+srv://martinr:fire76@mytineraycluster-df17o.mongodb.net/MYtineraryDB?retryWrites=true&w=majority',
  { useNewUrlParser: true }
);

let db = mongoose.connection;
db.once('open', () => console.log('Connected to the database'));
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//app.use('/api',passport.authenticate('jwt',{session: false}), cityCrud);
app.use('/api',  cityCrud);
app.use('/api', routeUser);
app.use('/api', require('./auth/authApis'));
app.use('/api/itineraries', itineraryCrud);
app.use('/api', activityCrud);

app.listen(8080, () => console.log('LISTENING ON PORT 8080'));
