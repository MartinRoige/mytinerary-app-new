var express = require('express');
var cors = require('cors');
var cityModel = require('./city');

const route = express.Router();
route.use(cors());

route.get('/cities', function(req, res) {
  cityModel.find().then(function(datos) {
    var cities = res.send(datos);
    return cities;
  });
});

route.get('/cities/:name', (req, res) => {
  let cityRequested = req.params.name;
  cityModel
    .findOne({ name: cityRequested })
    .then(city => {
      res.send(city);
    })
    .catch(err => console.log(err));
});

route.post('/cities/new', (req, res) => {
  const newCity = new cityModel({
    name: req.body.name,
    country: req.body.country,
    url: req.body.url
  });
  newCity
    .save()
    .then(city => {
      res.send(city);
    })
    .catch(err => {
      res.status(500).send('Server error');
    });
});

module.exports = route;
