var mongoose = require('mongoose');

const Schema = mongoose.Schema;

var itinerarySchema = new Schema(
  {
    cityId: Schema.Types.ObjectId,
    name: String,
    username: String,
    rating: 0,
    duration: String,
    cost: String,
    hashtags: [String],
    comments: {
      userId: Schema.Types.ObjectId,
      comment: String
    }
  },
  {
    collection: 'Itineraries'
  }
);

module.exports = mongoose.model('itinerary', itinerarySchema);
