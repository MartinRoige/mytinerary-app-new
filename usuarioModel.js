const mongoose = require('mongoose')

const Schema=mongoose.Schema;

var userSchema= new Schema({

userName: String,
password: String,
email: String,
profilePic: String,
firstName: String,
lastName: String,
country: String
},{
    collection: 'users'
})

module.exports = mongoose.model('user',userSchema)