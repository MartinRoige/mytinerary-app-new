var mongoose=require('mongoose');

const Schema=mongoose.Schema;

var citySchema= new Schema({
    name: String,
    country: String,
    url: String
},{
    collection: 'cities'
});

module.exports = mongoose.model('city', citySchema)