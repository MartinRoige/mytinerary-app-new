const express = require('express');
const router = express.Router();
const usuarioModel = require('./usuarioModel');
const jwt = require ('jsonwebtoken');
const key = require ('./secret.Key');
var cors = require('cors');


router.get('/users', function(req, res) {  
    usuarioModel.find().then(function(datos) {
      return res.send(datos);      
    })
});

router.post('/users/new', (req, res) => {
    const newUser = new usuarioModel({
      userName: req.body.userName,
      password: req.body.password,
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      country: req.body.country,
      profilePic: req.body.profilePic
    });
    newUser
      .save()
      .then(user => {
        res.send(user);
      })
      .catch(err => {
        res.status(500).send('Server error');
      });  
  });


router.post('/users/login',function(req,res){
    const userName = req.body.userName;
    usuarioModel.findOne({userName:userName})
        .then(user=>{
            if(!user){
                res.send({message: 'userName no existe '})
            }
            if (user.password===req.body.password){
                const payload = {
                    id: user.id,
                    userName: user.userName
                }
                const option = { expiresIn: '2592000'}
                jwt.sign(
                    payload,
                    key.secretOrKey,
                    option,
                    (err, token) => {
                        if (err){
                            res.json({
                                succes: false,
                                token: 'There was an error'
                            })                            
                        } else {
                            res.json({
                                succes: true,
                                token: token
                            })
                        }
                    }

                )
            } else {
                res.send({message: 'password incorrecto'})
            }
        })
        .catch(err=>{
            return res.send(err)
        })
        
})

module.exports = router