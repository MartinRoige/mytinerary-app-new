import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';
import './showActivities.css'

const ACTIVITY_URL = "http://localhost:8080/api/activities"

class ShowActivities extends React.Component {
    constructor(props) {
      super(props) 
      this.state = {
        activityList : []
      }
    } 

    
  
    async componentDidMount() {
      // await this.fetchActivities()
      let activities = await fetch(ACTIVITY_URL)
        .then((response) => response.json())
        // .then(result => this.setState({activityList : result }))    
        .catch(e => console.log(e))  
      let filterActivities = activities.filter(activity => {
        return (activity.itineraryId==this.props.Itinerary.itinerary._id)
      })   

      this.setState({activityList : filterActivities })
      console.log(this.state) 
      console.log(this.props.itinerary)  
      console.log(filterActivities)      
     

    }
  
    componentWillUnmount() {    
     
    }

    render(){
        return (
          <div>
            <h1>Activities</h1>
            <div className="activityCarrousel">
              <UncontrolledCarousel items={this.state.activityList}/>
            </div>
           
            
          </div>
    )}    
}

export default ShowActivities;

