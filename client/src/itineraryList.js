import React from 'react';
import './itineraryList.css';
import { Link, BrowserRouter as Router } from 'react-router-dom';
import { Row, Col, Container} from 'reactstrap';

const USERS_URL = 'http://localhost:8080/api/users';

class ItineraryList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {    
      userList: []    
    } 
  } 

  

  async componentDidMount() {
    let users = await fetch(USERS_URL)
    .then((response) => { return response.json()})
    //.then(result => this.setState({userList : result }))    
    .catch(e => console.log(e))     
    this.setState({userList : users})
  }

  componentWillUnmount() {    
  }

  render() {
  return this.props.list.map(list => 
    <div className='itineraryList'> 
      <Container>
        <Row>
        <Col xs="2" className="itineraryUser">
          <img className="itineraryUserImg"
            src={(this.state.userList.find((user) => {return user._id === list.userId })).profilePic}></img>
          <h6 className="itineraryUserName">
            { (this.state.userList.find((user) => {return user._id === list.userId })).userName}
          </h6>
        </Col>
        <Col xs="1">
        </Col>
        <Col xs="9" className="itineraryInfo">
          <Row>
            <Col>
              <h5 className="itinerayName"> {list.name }</h5>
            </Col>
          </Row>
          <Row>
            <Col>
              <p> Likes:{list.rating }</p>
            </Col>
            <Col>
              <p> {list.duration } Hours</p>
            </Col>
            <Col>
              <p> ${list.cost }</p>
            </Col>
          </Row>
          <Row>
            <Col>
              <p> {list.hashtags.map(hashtag =><span>{hashtag} </span>)} </p>
            </Col>
          </Row>
        </Col>
        </Row>        
      </Container>  
      <Link to={
            {
              pathname: '/itineraryActivities',
              myProps: {
                itinerary: list,
                cityName: this.props.cityName,
                cityImg:this.props.cityImg,
                userName: (this.state.userList.find((user) => {return user._id === list.userId })).userName ,
                userPic: (this.state.userList.find((user) => {return user._id === list.userId })).profilePic
              }
            }
          }>
        <p className='activityLink'>View All</p>
      </Link>                    
    </div>);
  } 

}

export default ItineraryList;
