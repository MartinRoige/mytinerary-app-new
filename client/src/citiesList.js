import React from 'react';
import './citieList.css';
import { Link, BrowserRouter as Router } from 'react-router-dom';

class CitiesList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return this.props.quotes.map(quote => (
      <div className='citieList'>
        <Link to={
            {
              pathname: `itineraries/list/${quote._id}`,
              myProps: quote,
              cityName: quote.name,
              cityImg: quote.url
            }
          }>
          <img
            className='citieListImg'
            src={quote.url}
            key={quote.name}
            alt={quote.name}
          />
        </Link>
        <h1 className='citieListName'>{quote.name}</h1>
      </div>
    ));
  }
}

export default CitiesList;
