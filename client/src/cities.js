import React from 'react';
import NavBar from './navbar';
import { Link, BrowserRouter as Router } from 'react-router-dom';
import homeIcon from './images/homeIcon.png';
import './cities.css';
import CitiesList from './citiesList';

const QUOTE_SERVICE_URL = 'http://localhost:8080/api/cities';

class Cities extends React.Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.state = {
      isFetching: false,
      quotes: [],
      cityFilter: []
    };
  }

  render() {
    return (
      <div className='cities'>
        <NavBar />
        <label className='labelCityFilter' htmlFor='filter'>
          Filter our current cities
        </label>
        <input
          className='inputCityFilter'
          type='text'
          id='filter'
          onChange={this.filterCities.bind(this)}
        />
        <CitiesList quotes={this.state.cityFilter} />
        <Link to='/'>
          <img src={homeIcon} className='homeIcon' alt='Home' />
        </Link>
      </div>
    );
  }

  componentDidMount() {
    this.fetchQuotes();
  }

  componentWillUnmount() {
    this.timer = null;
  }

  fetchQuotes = () => {
    this.setState({ ...this.state, isFetching: true });
    fetch(QUOTE_SERVICE_URL)
      .then(response => response.json())
      .then(result =>
        this.setState({ quotes: result, cityFilter: result, isFetching: false })
      )
      .catch(e => console.log(e));
  
  };

  filterCities = e => {
    let filteredCities = this.state.quotes;
    filteredCities = filteredCities.filter(city => {
      let cityName = city.name.toLowerCase();
      return cityName.indexOf(e.target.value.toLowerCase()) == 0;
    });
    this.setState({
      cityFilter: filteredCities
    });
  };
}

export default Cities;
