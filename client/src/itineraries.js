import React from 'react';
import NavBar from './navbar';
import homeIcon from './images/homeIcon.png';
import { Link, BrowserRouter as Router } from 'react-router-dom';
import './itineraries.css';
import ItineraryList from './itineraryList';
import axios from 'axios';



const LIST_URL = 'http://localhost:8080/api/itineraries/list/';

class Itineraries extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      list: [],   
      CITY_IMG : this.props.location.myProps.url,
      CITY_NAME : this.props.location.myProps.name
    }
  }

  render() {
    return (
      <div>
        <NavBar />
        <div className='itineraryCity'>
          <img className='citieListImg' src={this.state.CITY_IMG} />
        </div>
        <h1 className='citieListName'>{this.state.CITY_NAME}</h1>
        <p>Available MYtineraries:</p>

        <ItineraryList list={this.state.list} cityName={this.state.CITY_NAME} cityImg={this.state.CITY_IMG}/>        

        <Link to='../../cities'>
          <p className='chooseOther'>Choose another city...</p>
        </Link>
        <Link to='/'>
          <img src={homeIcon} className='homeIcon' alt='Home' />
        </Link>
      </div>
    );
  }

  componentDidMount() {
    this.fetchList();
  }

  componentWillUnmount() {
    this.timer = null;
  }

  fetchList = () => {
    this.setState({ ...this.state, isFetching: true });
    fetch(LIST_URL + this.props.match.params.id)
    .then(response => response.json())
    .then(result => this.setState({ list: result, isFetching: false }))
    .catch(e => console.log(e));    
  }

}

export default Itineraries;
