import React, {useState} from 'react';
import NavBar from './navbar';
import homeIcon from './images/homeIcon.png';
import {Link, BrowserRouter as Router } from 'react-router-dom';
import './createAccount.css';
import { Col, Button, Form, FormGroup, Label, Input, Modal, ModalBody, ModalHeader, ModalFooter} from 'reactstrap';

const USERS_URL = 'http://localhost:8080/api/users';
const SAVE_URL = 'http://localhost:8080/api/users/new';


class CreateAccount extends React.Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.createUser = this.createUser.bind(this);
    this.newuser = {    
        userName:'',
        password:'',
        email:'',
        firstName:'',
        lastName:'',
        country:'',
        profilePic:''
    }
    this.state = {
        userList: [],
        usernames: [],
        isFetching: false        
    }
    this.modalState = false 
  } 

  fetchUsers = () => {
    this.setState({ ...this.state, isFetching: true });
    fetch(USERS_URL)
      .then(response => response.json())
      .then(result =>
        this.setState({ userList: result, isFetching: false })
      )
      .catch(e => console.log(e));               
  }

  getUsernameList(){
     this.state.userList.map( user => this.state.usernames.push(user.userName))  
  }

  componentDidMount() {
    this.fetchUsers()          
  }

  componentWillUnmount() {
    this.timer = false;
  }

  onChange = (e) => {
    var user = this.newuser;
    user[e.target.name]=e.target.value;
    this.setState(user);  
  }

  createUser(){
    // fetch(SAVE_URL, {
    //     method: 'POST',
    //     headers: {
    //         Accept: "application/json",
    //         "Content-Type": "application/json"
    //       },    
    //     body: JSON.stringify(this.newuser)
    // })

    this.modalState = true;
        
    console.log(this.newuser)
    console.log(this.modalState)        
  }

 
  checkUser(){
      return (this.state.usernames.includes(this.newuser.userName) || (this.newuser.userName===""))
  }

  
  render() {
    
    return (   
             
        <div {...this.getUsernameList()}>            
            <NavBar />
            <h1>Create Account</h1>
                <Form className="createUserForm">
                    <FormGroup row>
                        <Label for="username" sm={3}>Username</Label>
                        <Col sm={9}>
                            { this.checkUser()?                                                         
                            <Input invalid type="text" name="userName" id="username" value={this.newuser.userName} onChange={this.onChange} />
                            :<Input valid type="text" name="userName" id="username" value={this.newuser.userName} onChange={this.onChange} />
                            }                      
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="password" sm={3}>Password</Label>
                        <Col sm={9}>
                            <Input type="password" name="password" id="password" value={this.newuser.password} onChange={this.onChange}/>
                        </Col>
                    </FormGroup>                    
                    <FormGroup row>
                        <Label for="email" sm={3}>Email</Label>
                        <Col sm={9}>
                            <Input type="email" name="email" id="email" value={this.newuser.email} onChange={this.onChange}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="firstname" sm={3}>First Name</Label>
                        <Col sm={9}>
                            <Input type="text" name="firstName" id="firstname" value={this.newuser.firstName} onChange={this.onChange}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="lastname" sm={3}>Last Name</Label>
                        <Col sm={9}>
                            <Input type="text" name="lastName" id="lastname" value={this.newuser.lastName} onChange={this.onChange}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="country" sm={3}>Country</Label>
                        <Col sm={9}>
                            <Input type="select" name="country" id="country" value={this.newuser.country} onChange={this.onChange}>
                                <option>Argentina</option>
                                <option>Italia</option>
                                <option>Perú</option>
                                <option>China</option>
                                <option>Japón</option>
                            </Input>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="profilepic" sm={3}>Profile Pic URL</Label>
                        <Col sm={9}>
                            <Input type="url" name="profilePic" id="profilepic" value={this.newuser.profilePic} onChange={this.onChange}/>
                        </Col>
                    </FormGroup>                   
                        {this.checkUser()?
                        <Button disabled>Submit</Button>
                        : <div>
                            <Button color="primary" onClick={this.createUser}>Submit</Button>
                            
                            <Modal isOpen={this.modalState}>
                            <ModalHeader > Create User Sussesful </ModalHeader>
                            <ModalBody> Redirecting to Login Page... </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.modalState=false}>Acept</Button>
                            </ModalFooter>
                            </Modal>
                         </div>
                        }
                </Form>
                
            <Link to="/">
                <img src={homeIcon} className="homeIcon" alt="Home" />
            </Link>            
        </div>
    )
  }   
}

export default CreateAccount;