import React from 'react';
import {Link, BrowserRouter as Router } from 'react-router-dom';
import logo from './images/MYtineraryLogo.png';
import logoStart from './images/circled-right-2.png';
import './home.css';
import 'bootstrap/dist/css/bootstrap.css';
import NavBar from './navbar';
import Popular from './popular';

function Home() {
  
  return (
   <div className="Home">
      <NavBar />
      <header className="Home-header">
        <img src={logo} className="Home-logo" alt="logo" />
        <p>
          Find your perfect trip, designed by insiders who know and love their cities.
        </p>
        <h3>Start Browsing</h3>
        <Link to="/cities">
          <img src={logoStart} className="logoStart" alt="Start" />
        </Link>
        <p>
          Popular MYtineraries
        </p>              
        <Popular />
      </header>
    </div>
  );
}

export default Home;