import React from 'react';
import NavBar from './navbar';
import ShowActivities from './showActivities'
import homeIcon from './images/homeIcon.png';
import { Link, BrowserRouter as Router } from 'react-router-dom';
import './itineraries.css';
import './itineraryList.css';
import { Row, Col, Container, FormGroup, Label, Input, Button} from 'reactstrap';

class ItineraryActivities extends React.Component {
    constructor(props) {
      super(props)
    }

    componentDidMount(){
        console.log(this.props.location.myProps)
    }
    
    render(){
        return (
            <div>
            <NavBar />
            <div className='itineraryCity'>
                <img className='citieListImg' src={this.props.location.myProps.cityImg} />
            </div>
            <h1 className='citieListName'>{this.props.location.myProps.cityName}</h1>
            <p>Available MYtineraries:</p>
            <div className='itineraryList'> 
                <Container>
                    <Row>
                    <Col xs="2" className="itineraryUser">
                    <img className="itineraryUserImg"
                        src={this.props.location.myProps.userPic}></img>
                    <h6 className="itineraryUserName">
                        { this.props.location.myProps.userName }
                    </h6>
                    </Col>
                    <Col xs="1">
                    </Col>
                    <Col xs="9" className="itineraryInfo">
                    <Row>
                        <Col>
                        <h5 className="itinerayName"> {this.props.location.myProps.itinerary.name }</h5>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                        <p> Likes:{ this.props.location.myProps.itinerary.rating }</p>
                        </Col>
                        <Col>
                        <p> {this.props.location.myProps.itinerary.duration } Hours</p>
                        </Col>
                        <Col>
                        <p> ${this.props.location.myProps.itinerary.cost }</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                        <p> {this.props.location.myProps.itinerary.hashtags.map(hashtag =><span>{hashtag} </span>)} </p>
                        </Col>
                    </Row>
                    </Col>
                    </Row>        
                </Container> 
            </div>
            
            <ShowActivities Itinerary = {this.props.location.myProps}/>

            <FormGroup>
                <Label for="commentText">Comments</Label>
                <Row>
                    <Col xs="9">
                        <Input type="textarea" name="commentText" id="commentText" placeholder="Your comment..."/>
                    </Col>
                    <Col xs="2">
                        <Button >Submit</Button>
                    </Col>
                </Row>
            </FormGroup>

            <Link to='../../cities'>
                <p className='chooseOther'>Choose another city...</p>
            </Link>  
            <Link to='/'>
                <img src={homeIcon} className='homeIcon' alt='Home' />
            </Link>
            </div> 
        )
    }

}

export default ItineraryActivities;