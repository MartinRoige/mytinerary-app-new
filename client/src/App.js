import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import Home from './home';
import Login from './login';
import Signin from './signin';
import Cities from './cities';
import Itineraries from './itineraries';
import CreateAccount from './createAccount';
import ItineraryActivities from './itineraryActivities'

class App extends React.Component {
  render() {
    return (
      <Router>
        <Route exact path='/' component={Home} />
        <Route path='/login' component={Login} />
        <Route path='/signin' component={Signin} />
        <Route path='/cities' component={Cities} />
        <Route path='/itineraries/list/:id' component={Itineraries} />
        <Route path='/createAccount' component={CreateAccount} />
        <Route path='/itineraryActivities' component={ItineraryActivities}/>
      </Router>
    );
  }
}

export default App;
