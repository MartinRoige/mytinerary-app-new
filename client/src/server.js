
var express=require('express');
var mongoose=require('mongoose');
var cors=require('cors');
var cityModel=require('./city');

const app=express();
app.use(cors());

mongoose.connect('mongodb+srv://martinr:fire76@mytineraycluster-df17o.mongodb.net/MYtineraryDB?retryWrites=true&w=majority',{useNewUrlParser: true});

let db = mongoose.connection;
db.once('open', () => console.log('Connected to the database'));
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.get('/app/cities', function (req,res){
    cityModel.find()
    .then(
        function(datos){            
            return res.send(datos)    
    })
});

app.listen(8080,() => console.log('LISTENING ON PORT 8080'));

