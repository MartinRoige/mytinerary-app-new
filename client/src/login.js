import React from 'react'
import NavBar from './navbar';
import './login.css';
import homeIcon from './images/homeIcon.png';
import {Link, BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios'
import { Col, Button, Form, FormGroup, Label, Input} from 'reactstrap';

const USERS_URL = 'http://localhost:8080/api/users';
const USERS_LOGIN_URL = 'http://localhost:8080/api/users/login';

class Login extends React.Component {

  constructor (props) {
    super(props)
    this.state = {    
      username:'',
      password:'',
      userList: [],
      usernames: [],
      isFetching: false 
    }    
  }

  fetchUsers = () => {
    this.setState({ ...this.state, isFetching: true });
    fetch(USERS_URL)
      .then(response => response.json())
      .then(result =>
        this.setState({ userList: result, isFetching: false })
      )
      .catch(e => console.log(e));               
  }

  componentDidMount() {
    this.fetchUsers()          
  }

  componentWillUnmount() {
    this.timer = false;
  }

  onChange = (e) => {
    var state = this.state;
    state[e.target.name]=e.target.value;
    this.setState(state);  
  }

  onSave = () => {
    axios.post();
  }

  signGoogle = () => {

      window.location.href = 'http://localhost:8080/api/auth/google'

  }

  signIn = () => {

    // fetch(USERS_LOGIN_URL, {body: this.state})
    // .then(response => response.json())
    // .then(result => console.log(result))

    // this.state.userList.map( user => this.state.usernames.push(user.userName))

    // if (this.state.usernames.includes(this.state.username)) {
    //   const checkuser = this.state.userList.find( user => user.userName===this.state.username);
    //   if (checkuser.password===this.state.password) {
    //     console.log('Ingreso Correcto')
    //   } else {
    //     console.log('contraseña incorrecta')
    //   }
    // } else {
    //   console.log('Usuario incorrecto')
    // }
  }

  render() {
    return (
      <div>
          <NavBar />
          <h1>Login</h1>
          <Form className="loginForm">
            <FormGroup row>
              <Label for="username" sm={3}>Username</Label>
              <Col sm={9}>
                <Input name='username' value={this.state.username} type='text' onChange={this.onChange}/>  
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="password" sm={3}>Password</Label>
              <Col sm={9}>
                <Input name='password' value={this.state.password} type='password' onChange={this.onChange}/>
              </Col>
            </FormGroup> 
          
            <Button color="primary" onClick={this.signIn}>Sign in</Button> {' '}
            <Button color="primary" onClick={this.signGoogle}>Sign With Google</Button> {' '}
            
          </Form>
          <Link to="/">
            <img src={homeIcon} className="homeIcon" alt="Home" />
          </Link>
      </div>
    )    
  }
}

export default Login